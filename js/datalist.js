function DataList(element, options) {

    // Defined Elements
    var currentElement = $(element);
    var containerElement = $('<div>').addClass('form-control datalist-container');
    var dropElement = $('<div>').addClass('datalist-drop');
    var searchElement = $('<div>').addClass('datalist-search');
    var inputSearchElement = $('<input type="text"/>').addClass('form-control');
    var resultsElement = $('<ul>').addClass('datalist-results');

    // Defined Variables
    var inputDelay;
    var activeIndex = 0;

    // Defined Functions
    var init = function() {
        searchElement.html(inputSearchElement);
        searchElement.append('<div><i class="glyphicon glyphicon-search"></i></div>');
        dropElement.append(resultsElement);
        containerElement.append(searchElement);
        containerElement.append(dropElement);

        inputSearchElement.val(currentElement.val());

        inputSearchElement.val(options.default ? options.default : "");
        currentElement.val(options.default ? options.default : "");

        if (options && options['attrs']) {
            var attrs = options['attrs'];
            for (attr in attrs) {
                if (attr === 'class') {
                    containerElement.addClass(attrs[attr]);
                    continue;
                }
                containerElement.attr(attr, attrs[attr]);
            }
        }

        containerElement.keydown(function(event) {
            if (event.keyCode === 27) {
                hide(function() {
                    inputSearchElement.focus();
                });
            }
        });

        if (options && options['direction'] && options['direction'] === 'rtl')
            containerElement.addClass('datalist-rtl');

        inputSearchElement.focusout(function() {
            if (dropElement.css('display') === 'block') {
                hide();
            }
        });

        // For Unsubmited form
        inputSearchElement.keydown(function(event) {
            var liCount = resultsElement.children('li').length;
            var resultsElementHeight = $(resultsElement).height();
            var liHeight = resultsElement.children('li.active').outerHeight(true);

            switch (Number(event.keyCode)) {
                case 13:
                    if (activeIndex === 0) {
                        active(1);
                    }
                    select(resultsElement.children('li.active'));
                    return false;
                case 40:
                    if (activeIndex >= liCount) {
                        active(1);
                    }
                    else {
                        active(activeIndex + 1);
                    }
                    resultsElement.scrollTop(liHeight * activeIndex - resultsElementHeight);
                    return false;
                case 38:
                    if (activeIndex <= 1) {
                        active(liCount);
                    }
                    else {
                        active(activeIndex - 1);
                    }
                    resultsElement.scrollTop(liHeight * activeIndex - resultsElementHeight);
                    return false;
            }

        });

        inputSearchElement.bind('input', function() {
            currentElement.val(inputSearchElement.val());

            if (inputSearchElement.val() === "") {
                hide();
                return true;
            }

            if (inputDelay !== undefined) {
                clearTimeout(inputDelay);
                inputDelay = undefined;
            }
            else {
                inputDelay = setTimeout(function() {
                    find(inputSearchElement.val());
                    inputDelay = undefined;
                }, options.delay ? options.delay : 500);
            }
        });

        resultsElement.on('click', 'li', function() {
            select(this);
        });

        currentElement.change(function() {
            if (currentElement.val() !== inputSearchElement.val()) {
                inputSearchElement.val(currentElement.val());
            }
        });

        //Hidden Select element and insert single select component
        currentElement.css('display', 'none');
        currentElement.after(containerElement);

    };

    var find = function(text) {
        var itemElement;
        if (!options && !options.hasOwnProperty('fetch'))
            return false;

        options['fetch'](text, function(result) {
            if (!(result instanceof Array)) {
                console.error('Result not Array');
                return false;
            }

            resultsElement.empty();

            if (result.length === 0) {
                itemElement = $('<li>')
                    .html(options.message)
                    .addClass('datalist-message')
                    .click(function(){return false;});
                resultsElement.append(itemElement);
            }


            if (typeof result[0] === 'object') {
                result.forEach(function(item) {
                    itemElement = $('<li>').html(item.content).data('item', item);
                    resultsElement.append(itemElement);
                });
            }
            else {
                result.forEach(function(item) {
                    itemElement = $('<li>').text(item);
                    resultsElement.append(itemElement);
                });
            }
            active(1);
            show();
        });
    };

    var show = function() {
        if(currentElement.hasClass('datalist-dropdown')) {
            dropElement.css('top', containerElement.height());
        }
        else if(currentElement.hasClass('datalist-dropup')) {
            dropElement.css('bottom', containerElement.height() + 2);
        }
        else if(currentElement.hasClass('datalist-dropleft')) {
            dropElement.css('right', containerElement.width() + 2);
            dropElement.css('top', -2);
        }
        else if(currentElement.hasClass('datalist-dropright')) {
            dropElement.css('left', containerElement.width() + 2);
            dropElement.css('top', -2);
        }

        dropElement.fadeIn('fast', function() {
            inputSearchElement.focus();
        });
    };

    var hide = function(callback) {
        dropElement.fadeOut('fast', callback);
    };

    var select = function(selectedElement) {

        if (!options && !options.hasOwnProperty('onSelected'))
            return false;

        var item;

        if ($(selectedElement).data('item')) {
            item = $(selectedElement).data('item');
            inputSearchElement.val(item.caption);
            currentElement.val(item.value ? item.value : item.caption);
        }
        else {
            item = $(selectedElement).text();
            inputSearchElement.val(item);
            currentElement.val(item);
        }

        options['onSelected'](item);

        hide(function() {
            inputSearchElement.focus();
        });
    };

    var active = function(index) {
        if (index)
            activeIndex = index;

        resultsElement.children('li.active').removeClass('active');
        resultsElement.children('li:nth-child(' + activeIndex + ')').addClass('active');
    };

    init();

    return {};
}

/**
 *
 * @param {Object} options
 * @returns {$.fn@call;each}
 */
$.fn.dataList = function(options) {
    return this.each(function() {
        var $this = $(this);
        var list = $this.data('list');
        if (!list) {
            $this.data('list', (list = DataList(this, options)));
        }
    });
};
